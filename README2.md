1. This log is different than what was found on the console due to the fact that the console only prints messages at log level info. The file log on the other hand prints logs of all levels and dumps this information to a file. 

2. This exception comes from the ConditionEvaluator class. 

3. AssertThrows checks to see if an expression throws a specific exception. After checking this, it throws the same exception. 

4. 
    a. The serialVersionUID is a unique identifier for the Serializable class. It's used to ensure that a deserialized object matches a loaded class. 
    b. We need to override constructors since TimeException is a subclass ofException. Since we do not want the default Exception constructor, we must override this.  
    c. We do not override other Exception methods since we want their default behavior and as such have no need to change these functions. 

5. The static block defines functionality that must be treated as static and therefore called only one time once the class has been called. It allows for the logger to be set up for this class and so any class instance of Timer will already have the log file set up and ready to use. 

6. README.md is a markdown file. This file type allows for stylistic information to be put into text documents. The README.md markdown file is always displayed below the code in the bitbucket repo. This allows for people to read relavent information about the project such as what it does, how to start it, and author information. 

7. The Time class should be throwing a TimerException but is istead throwing a null pointer exception. We need to change the order of operations preformed. 

8. The actual issue is that the finally block is being excuted before the TimerException is thrown. This results in a null pointer exception being thrown since timeNow has not been initialized before the program tries to throw the TimerException which results in the finally block being called before the variable has been created leading to the crash. 

9. NullPointerException is an UnCheckedException whereas TimerException is a CheckedException.   
